'use strict';
/*
 * Twilio support
 */

function RetrieveTwilioToken(client) {
    return $.ajax({
        url: "/twilio/",
        data: {auth_code: client.AuthCode()},
        method: "POST"
    }).then(function(data) {
        return data.Token;
    });
}

/*
 * Wallpapers
 */

var wallpapers = {
    choose: function() {
        var wallpapers = [
            'http://trails.develer.net:8000/_DSC7035.jpg',
            'http://trails.develer.net:8000/_DSC7037.jpg',
            'http://trails.develer.net:8000/_DSC7040.jpg',
            'http://trails.develer.net:8000/_DSC7059.jpg',
            'http://trails.develer.net:8000/_DSC7061.jpg',
            'http://trails.develer.net:8000/_DSC7078.jpg',
            'http://trails.develer.net:8000/_DSC7095.jpg',
            'http://trails.develer.net:8000/_DSC7152.jpg'
        ];
        var ix = Math.floor(Math.random() * wallpapers.length);
        return wallpapers[ix];
    }
};

/*
 * GUI code
 */

var client;
var view_width;

var twilio_hooks = {
    disconnect: null
};

/* GUI utility */

function log() {
    console.log.apply(window.console, arguments);
};

var change_background = function() {
    document.body.style.backgroundImage = "url(" + wallpapers.choose() + ")";
};

function ShowBtn(id, enabled) {
    var btn = $(document.getElementById(id));
    if (enabled) {
        btn.removeClass("hide");
    }
    else {
        btn.addClass("hide");
    }
}

function ShowView(name) {
    var pos = -1;
    $(".views .view").each(function(index, dom) {
        if ($(dom).hasClass(name)) {
            pos = index;
        }
    });

    if (pos == -1) {
        throw new Error("view not found");
    }
    var w = $(".views > .wrapper").get(0);
    w.style.marginLeft = -(pos * view_width) + "px";
}

/* GUI modals */

function SessionModal() {
    var dlg = $("#SessionModal");
    dlg.find("form")
        .on("submit", function(e) {
            dlg.find(".btn.btn-primary").click();
            return false;
        });
    var telephone = dlg.find("input[name=telephone]");
    var d = Q.defer();
    dlg.modal({keyboard: false})
        .on('hidden.bs.modal', function (e) {
            var number = telephone.val();
            if (number.length > 0) {
                d.resolve(number);
            }
            else {
                d.reject();
            }
        });
    return d.promise;
};

function ValidateSessionModal() {
    var dlg = $("#ValidateSessionModal");
    dlg.find("form")
        .on("submit", function(e) {
            dlg.find(".btn.btn-primary").click();
            return false;
        });
    var code = dlg.find("input[name=code]");
    var d = Q.defer();
    dlg.modal({keyboard: false})
        .on('hidden.bs.modal', function (e) {
            var value = code.val();
            if (value.length > 0) {
                d.resolve(value);
            }
            else {
                d.reject();
            }
        });
    return d.promise;
};

function ErrorModal(text) {
    var dlg = $("#ErrorModal");
    dlg.find("p.text").text(text);
    dlg.modal({keyboard: false});
};

/* GUI views */

var RendezvousView = {
    init: function() {
        $("#HangupBtn").on("click", function() {
            Twilio.Device.disconnectAll();
        });

        $("#NewThreadBtn").on("click", function() {
            ShowBtn("NewThreadBtn", false);
            RendezvousView.thread_workflow(client)
                .then(function() {
                    ShowBtn("NewThreadBtn", true);
                });
        });
    },
    show: function() {
        ShowView("rendezvous");
    },
    thread_workflow: function() {
        return client.NewThread()
            .then(function(data) {
                ShowBtn("HangupBtn", true);
                log("new thread created, calling twilio...", data)
                return RendezvousView.twilio_call({"Otp": data.otp})
                    .then(function() {
                        ShowBtn("HangupBtn", false);
                    });
            });
    },
    twilio_call: function(opts) {
        var d = Q.defer();
        var t0 = new Date()
        twilio_hooks.disconnect = function(conn) {
            var t = (new Date()) - t0;
            var secs = Math.floor(t / 1000);
            log("call finished in ", secs);
            d.resolve(secs);
        };
        Twilio.Device.connect(opts);
        return d.promise;
    }
};

var FeedbackView = {
    init: function() {
        $("#FeedbackZero").click(function() {
            RendezvousView.show();
        });

        $("#FeedbackOk").click(function() {
            SecondChanceView.show(FeedbackView.thread_id);
        });

        $("#FeedbackNo").click(function() {
            client.ThreadFeedback(FeedbackView.thread_id, {feedback: "-1"});
            // non c'è bisogno di aspettare che il feedback sia stato ricevuto
            // per cambiare vista
            RendezvousView.show();
        });
    },
    show: function(thread_id) {
        this.thread_id = thread_id;
        ShowView("feedback");
    }
};

var SecondChanceView = {
    init: function() {
        client.Profile()
            .then(function(r) {
                var t = r.data.second_chance || "";
                $(".second_chance textarea").val(t);
            });
        $("#SecondChanceOk").click(function() {
            var text = $(".second_chance textarea").val();
            if (text.length == 0) {
                alert("please entere a message to send");
                return;
            }
            client.Profile({second_chance: text})
                .then(function() {
                    client.ThreadFeedback(SecondChanceView.thread_id, {feedback: "+1"});
                });
            ShowView("rendezvous");
        });
        $("#SecondChanceNo").click(function() {
            ShowView("rendezvous");
        });
    },
    show: function(thread_id) {
        change_background();
        this.thread_id = thread_id;
        ShowView("second_chance");
    }
};

var IncomingView = {
    init: function() {
        $("#IncomingOk").click(function() {
            IncomingView.accept();
        });
        $("#IncomingNo").click(function() {
            IncomingView.hangup();
        });
    },
    show: function(connection) {
        this.connection = connection;
        this.connection_started = null;
        ShowView("incoming");
    },
    _show_text: function() {
        $(".incoming .two-buttons-wrapper").get(0).style.marginLeft = -view_width + "px";
    },
    _reset_text: function() {
        $(".incoming .two-buttons-wrapper").get(0).style.marginLeft = "0";
    },
    accept: function() {
        var me = this;
        this.connection.disconnect(function(c) {
            var  t = (new Date()) - me.connection_started;
            var secs = Math.floor(t / 1000);
            me._connection_closed(secs);
        });
        this.connection_started = new Date();
        this.connection.accept();
        this._show_text();
    },
    hangup: function() {
        if (this.connection_started == null) {
            // call never started
            this.connection.reject();
            return;
        }
        else {
            // call in progress
            this.connection.disconnect();
        }
    },
    _connection_closed: function(elasped) {
        if (elasped < 5) {
            this._reset_text();
            RendezvousView.show();
            return
        }
        this._reset_text();
        client.ThreadsList()
            .then(function(threads) {
                var last = threads[0].id;
                FeedbackView.show(last);
            });
    }
};

var OfflineView = {
    init: function() {
    },
    show: function() {
        ShowView("offline");
    }
};
/* workflows */

// login_workflow implements the steps needed to login an user.
// When the returned promise is resolved the user is logged in and the client
// authorized.
var login_workflow = function(client) {
    return client.Load()
        .then(function(loaded) {
            if (loaded) {
                // verify the loaded session against the server
                return client.Profile()
                    .then(function() {
                        return true;
                    })
                    .fail(function() {
                        // session scaduta?
                        log("reset");
                        client.Reset();
                        return client.Save()
                            .then(function() {
                                return login_workflow(client);
                            })
                    });
            }
            else {
                return SessionModal()
                    .then(function(number) {
                        log("new session needed for", number);
                        return client.NewSession(number);
                    })
                    .then(function(response) {
                        var session_id = response.id;
                        var client_name = response.client;
                        return ValidateSessionModal()
                            .then(function(code) {
                                return [session_id, client_name, code];
                            });
                    })
                    .then(function(data) {
                        var session_id = data[0];
                        var client_name = data[1];
                        var code = data[2];
                        log("try to validate the new session with the user supplied code", code);
                        return client.ValidateSession(session_id, code)
                            .then(function(data) {
                                return [data.authcode, client_name];
                            });
                    })
                    .then(function(data) {
                        var authcode = data[0];
                        var client_name = data[1];
                        client.AuthCode(authcode, client_name);
                    })
                    .then(function() {
                        log("session ready: ", client.Status());
                        return client.Save();
                    })
                    .fail(function() {
                        console.log(arguments);
                        ErrorModal("Error trying to create a new session");
                        throw new Error("login");
                    });
            }
        });
};

/* main */
(function() {
    change_background();
    /* one time setup for the gui (needed to support screens of differt size) */
    view_width = $(".views").width();
    $(".views .view").width(view_width);
    $(".two-buttons-wrapper > div").width(view_width);

    client = new (ThreadsClient.browserClient())(window.THREADS_URL);
    login_workflow(client)
        .then(function() {
            log("login done", client.ClientName());
            RendezvousView.init();
            FeedbackView.init();
            SecondChanceView.init();
            IncomingView.init();
            OfflineView.init();
        })
        .then(function() {
            return RetrieveTwilioToken(client)
        })
        .then(function(token) {
            Twilio.Device.setup(token);

            Twilio.Device.ready(function(device) {
                log("Twilio ready");
            });

            Twilio.Device.error(function(error) {
                log("TWILIO Error: " + error.message);
            });

            Twilio.Device.offline(function(device) {
                log("TWILIO offline!");
                OfflineView.show();
            });

            Twilio.Device.incoming(function(conn) {
                log("TWILIO incoming!");
                IncomingView.show(conn);
            });

            Twilio.Device.connect(function(conn) {
                log("TWILIO device connected");
            });

            Twilio.Device.disconnect(function(conn) {
                log("TWILIO disconnected!");
                if (twilio_hooks.disconnect) {
                    var fn = twilio_hooks.disconnect;
                    twilio_hooks.disconnect = null;
                    fn(conn);
                }
            });
        })
        .then(function() {
            setInterval(function() {
                client.Profile();
            }, 60000);
        })
        .then(function() {
            RendezvousView.show();
        });
})();
