package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/go-martini/martini"
	"github.com/martini-contrib/render"

	"github.com/dgrijalva/jwt-go"
)

// incomplete porting of the official twilio-php/Services/Twilio/Capability.php
func http_build_query(params map[string]string) string {
	var encoded []string
	for name, value := range params {
		encoded = append(encoded, url.QueryEscape(name)+"="+url.QueryEscape(value))
	}
	return strings.Join(encoded, "&")
}

type ScopeURI struct {
	service   string
	privilege string
	params    map[string]string
}

func (self ScopeURI) String() string {
	uri := url.URL{Scheme: "scope", Opaque: fmt.Sprintf("%s:%s", self.service, self.privilege)}
	if len(self.params) > 0 {
		uri.RawQuery = http_build_query(self.params)
	}
	return uri.String()
}

func NewScopeURI(service, privilege string, params map[string]string) ScopeURI {
	return ScopeURI{service, privilege, params}
}

type Capability struct {
	accountSid string
	authToken  string
	scopes     []ScopeURI
}

/*
 * Allow the user of this token to make outgoing connections.
 *
 * app_sid the application to which this token grants access
 * app_params signed parameters that the user of this token cannot overwrite.
 */
func (self *Capability) allowClientOutgoing(app_sid string, app_params map[string]string) {
	params := make(map[string]string)
	params["appSid"] = app_sid
	params["appParams"] = http_build_query(app_params)
	self.scopes = append(self.scopes, NewScopeURI("client", "outgoing", params))
}

/*
 * Allow the user of this token to receive incoming connections.
 *
 * client_name the name the client will be reachable.
 */
func (self *Capability) allowClientIncoming(client_name string) {
	params := make(map[string]string)
	params["clientName"] = client_name
	self.scopes = append(self.scopes, NewScopeURI("client", "incoming", params))
}

/*
 * Generates a new token based on the credentials and permissions that
 * previously has been granted to this token.
 *
 * return the newly generated token that is valid for $ttl seconds
 */
func (self Capability) GenerateToken() string {
	return self.GenerateTokenWithDuration(1 * time.Hour)
}
func (self Capability) GenerateTokenWithDuration(d time.Duration) string {
	token := jwt.New(jwt.SigningMethodHS256)
	token.Claims["iss"] = self.accountSid
	token.Claims["exp"] = time.Now().Add(d).Unix()

	var client_name string
	for _, s := range self.scopes {
		if s.service == "client" && s.privilege == "incoming" {
			client_name = s.params["clientName"]
		}
	}

	var scopes []string
	for _, s := range self.scopes {
		if s.service == "client" && s.privilege == "outgoing" && client_name != "" {
			s.params["clientName"] = client_name
		}
		scopes = append(scopes, s.String())
	}
	token.Claims["scope"] = strings.Join(scopes, " ")
	tokenString, err := token.SignedString([]byte(self.authToken))
	if err != nil {
		panic(err)
	}
	return tokenString
}

func NewCapability(sid, token string) Capability {
	return Capability{accountSid: sid, authToken: token}
}

type TwilioConfig struct {
	AccountSID string
	AuthToken  string
	AppSID     string
}

func (self *TwilioConfig) LoadFromEnv() {
	self.AccountSID = os.Getenv("TWILIO_SID")
	self.AuthToken = os.Getenv("TWILIO_TOKEN")
	self.AppSID = os.Getenv("TWILIO_APP_SID")
	if self.AccountSID == "" {
		log.Fatalln("TWILIO_SID is empty")
	}
	if self.AuthToken == "" {
		log.Fatalln("TWILIO_TOKEN is empty")
	}
	if self.AppSID == "" {
		log.Fatalln("TWILIO_APP_SID is empty")
	}
}

type ThreadsConfig struct {
	URL string
}

func (self *ThreadsConfig) LoadFromEnv() {
	self.URL = os.Getenv("THREADS_URL")
	if self.URL == "" {
		log.Fatalln("THREADS_URL is empty")
	}
}

func main() {
	twilio := TwilioConfig{}
	twilio.LoadFromEnv()

	threads := ThreadsConfig{}
	threads.LoadFromEnv()

	m := martini.Classic()
	m.Use(martini.Static("dist", martini.StaticOptions{Prefix: "/dist", SkipLogging: true}))
	m.Use(render.Renderer())
	m.Get("/", func(r render.Render) {
		ctx := struct {
			ThreadsURL string
		}{
			threads.URL,
		}
		r.HTML(200, "index", ctx)
	})
	m.Post("/twilio/", func(w http.ResponseWriter, r *http.Request) {
		auth_code := r.FormValue("auth_code")
		if auth_code == "" {
			w.WriteHeader(400)
			return
		}

		req, err := http.NewRequest("GET", threads.URL+"v1beta/me", nil)
		if err != nil {
			log.Println(err)
			w.WriteHeader(500)
			return
		}
		req.SetBasicAuth("", auth_code)

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			log.Println(err)
			w.WriteHeader(500)
			return
		}
		defer resp.Body.Close()

		type ProfileResponse struct {
			Telephone    string `json:"telephone"`
			SecondChance string `json:"second_chance"`
			Session      struct {
				Client string `json:"client"`
			}
		}
		profile := ProfileResponse{}
		err = json.NewDecoder(resp.Body).Decode(&profile)
		if err != nil {
			log.Println(err)
			w.WriteHeader(500)
			return
		}

		tc := NewCapability(twilio.AccountSID, twilio.AuthToken)
		tc.allowClientOutgoing(twilio.AppSID, nil)
		tc.allowClientIncoming(profile.Session.Client)
		token := tc.GenerateToken()

		bytes, err := json.Marshal(struct{ Token string }{token})
		if err != nil {
			log.Println(err)
			w.WriteHeader(500)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(bytes)
	})
	m.Run()
}
