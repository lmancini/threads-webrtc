var gulp = require('gulp');

gulp.task('default', function() {
    gulp.watch('./assets/**/*', ['app']);
});

gulp.task('build', ['app', 'libs'], function() {});

gulp.task('app', function() {
    gulp.src('./assets/**/*')
        .pipe(gulp.dest('./dist/app/'));
});

gulp.task('libs', ['bootstrap', 'jquery', 'q', 'threads_client'], function() {});

gulp.task('bootstrap', function() {
    gulp.src('./bower_components/bootstrap/dist/**/*')
        .pipe(gulp.dest('./dist/libs/bootstrap/'));
});

gulp.task('jquery', function() {
    gulp.src('./bower_components/jquery/dist/jquery.min.js')
        .pipe(gulp.dest('./dist/libs/jquery/'));
});

gulp.task('q', function() {
    gulp.src('./bower_components/q/q.js')
        .pipe(gulp.dest('./dist/libs/q/'));
});

gulp.task('threads_client', function() {
    gulp.src('./threads-client-js/dist/browser/threads_client.min.js')
        .pipe(gulp.dest('./dist/libs/threads_client/'));
});
