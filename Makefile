.PHONY: clean build deploy

build: clean
	gulp build
	go build

clean:
	rm -rf dist

deploy: build
	ssh threads@trails.develer.net mkdir -p webrtc/
	ssh threads@trails.develer.net mv webrtc/threads-webrtc threads-webrtc.running || true
	ssh threads@trails.develer.net rm -rf webrtc/templates
	ssh threads@trails.develer.net rm -rf webrtc/dist
	scp -r ./templates threads@trails.develer.net:webrtc/
	scp -r ./dist threads@trails.develer.net:webrtc/
	scp threads-webrtc threads@trails.develer.net:webrtc/
	ssh threads@trails.develer.net rm threads-webrtc.running || true
	ssh threads@trails.develer.net killall threads-webrtc || true
